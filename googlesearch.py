from googleapiclient.discovery import build
import settings

service = build("customsearch", "v1", developerKey=settings.GOOGLE_SEARCH_API_KEY)
custom_search_engine = service.cse()


def search_google_for_term(search_term: str) -> list:
    results_json = custom_search_engine.list(
        q=search_term,
        num=5,
        cx=settings.GOOGLE_CUSTOM_SEARCH_ENGINE_ID
    ).execute()
    return [(item["title"], item["link"]) for item in results_json["items"]]