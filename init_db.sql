CREATE DATABASE google_search_bot_db;
USE google_search_bot_db;
CREATE TABLE IF NOT EXISTS search_history (search_user VARCHAR(255), search_term VARCHAR(255), timestamp TIMESTAMP);