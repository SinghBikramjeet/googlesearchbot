
from main import bot_listen
from flask import Flask
app = Flask(__name__)

@app.route('/ping')
def ping():
    return 'pong'

bot_listen()