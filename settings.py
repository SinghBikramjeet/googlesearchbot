from dotenv import load_dotenv
import os

load_dotenv()
TOKEN: str = os.getenv('DISCORD_TOKEN')
DATABASE_HOST: str = os.getenv('DATABASE_HOST')
DATABASE_NAME: str = os.getenv('DATABASE_NAME')
DATABASE_USERNAME: str = os.getenv('DATABASE_USERNAME')
DATABASE_PASSWORD: str = os.getenv('DATABASE_PASSWORD')
GOOGLE_SEARCH_API_KEY: str = os.getenv('GOOGLE_SEARCH_API_KEY')
GOOGLE_CUSTOM_SEARCH_ENGINE_ID: str = os.getenv('GOOGLE_CUSTOM_SEARCH_ENGINE_ID')
