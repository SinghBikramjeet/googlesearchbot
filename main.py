from dbhelper import db_conn
from bot import prepare_bot
from client import GoogleSearchClient
import settings

def bot_listen():
    with db_conn:
        google_search_bot = GoogleSearchClient(command_prefix='!')
        prepare_bot(google_search_bot)
        google_search_bot.run(settings.TOKEN)
