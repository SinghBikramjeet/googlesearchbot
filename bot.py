from discord.ext.commands import Bot, Context
import dbhelper
from models import SearchInstance
from googlesearch import search_google_for_term


def prepare_bot(bot: Bot):

    @bot.command(name='hi')
    async def say_hi(ctx: Context):
        await ctx.send('hey')

    @bot.command(name='google')
    async def search_google(ctx: Context, *args):
        search_term = " ".join(args)
        dbhelper.save_search(
            SearchInstance(search_user=str(ctx.author), search_term=search_term)
        )
        search_results: list = search_google_for_term(search_term)
        response = "Your search results are:\n" + '\n'.join(
            [f"\n{result[0]}\n{result[1]}" for result in search_results]
        )
        await ctx.send(response)

    @bot.command(name='recent')
    async def get_history(ctx: Context, *args):
        search_history: list = dbhelper.get_search_history(str(ctx.author), " ".join(args))
        response = "Your recent searches are:\n" + '\n'.join(search_history)
        await ctx.send(response)
