from datetime import datetime


class SearchInstance:

    def __init__(self, search_user: str, search_term: str):
        self.search_user = search_user
        self.search_term = search_term.lower()
        self.timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
