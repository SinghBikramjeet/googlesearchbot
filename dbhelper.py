import mysql.connector
import settings
from models import SearchInstance

db_conn = mysql.connector.connect(
    host=settings.DATABASE_HOST,
    database=settings.DATABASE_NAME,
    user=settings.DATABASE_USERNAME,
    password=settings.DATABASE_PASSWORD,
)


def save_search(search_instance: SearchInstance):
    query = f"""
    INSERT INTO search_history (search_user, search_term, timestamp)
    VALUES ('{search_instance.search_user}', '{search_instance.search_term}', '{search_instance.timestamp}');
    """

    cursor = db_conn.cursor()
    cursor.execute(query)
    db_conn.commit()


def get_search_history(search_user: str, search_term: str = '') -> list:
    query = f"""
    SELECT search_term
    FROM search_history
    WHERE search_user='{search_user}'
    AND search_term LIKE '%{search_term}%'
    ORDER BY timestamp DESC;
    """

    cursor = db_conn.cursor()
    cursor.execute(query)
    return [item[0] for item in cursor.fetchall()]